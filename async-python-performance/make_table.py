import os

current_dir = os.path.dirname(os.path.abspath(__file__))
log_dir = os.path.join(current_dir, "ab_logs")


def get_result_fields(file_text, field_names):
    if hasattr(field_names, "split"):
        field_names = [field_names]
    results = {}
    for line in file_text.split("\n"):
        for field_name in field_names:
            if field_name in line:
                splitted = line.split()
                for val in splitted:
                    try:
                        results[field_name] = float(val)
                    except ValueError as err:
                        pass

    return results


def format_table(table_list, header_names=None, row_names=None):
    table_str = ["<table>\n\t<tr>"]
    for hdr_name in header_names:
        table_str.append("\t\t<th>{}</th>".format(hdr_name))
    table_str.append("\t</tr>")
    for i, row in enumerate(table_list):
        row_str = ["\t<tr>"]
        row_str.append("\t\t<td>{}</td>".format(row_names[i]))
        for val in row:
            row_str.append("\t\t<td>{:.2f}</td>".format(val))
        row_str.append("\t</tr>")
        table_str.append("\n".join(row_str))
    table_str.append("</table>")
    return "\n".join(table_str)


def main():
    req_per_sec = "Requests per second"

    row_names = ["dev", "gunicorn", "gunicorn.w4"]
    impl_names = ["aiohttp", "flask"]
    table = []
    ref_row = 0
    for i, row_name in enumerate(row_names):
        row = []
        for impl in impl_names:
            file_name = f"ab.{impl}.{row_name}.log"
            file_path = os.path.join(log_dir, file_name)
            with open(file_path, "r") as f:
                file_text = f.read()
                results = get_result_fields(file_text, req_per_sec)
            row.append(results[req_per_sec])
        row.append(((row[0] / row[1]) - 1)*100)
        table.append(row)
        if i != ref_row:
            delta_row = []
            for i in range(len(row)):
                delta_row.append(((row[i] / table[ref_row][i]) - 1)*100)
            table.append(delta_row)

    table_str = format_table(
        table,
        header_names=["", "`aiohttp`", "`Flask`", "% difference"],
        row_names=[
            "Development server (Requests/sec)",
            "`gunicorn` (Requests/sec)",
            "% increase over development server",
            "`gunicorn -w 4` (Requests/sec)",
            "% increase over development server",
        ]
    )
    with open("table.html", "w") as f_html:
        f_html.write(table_str)


if __name__ == '__main__':
    main()
