import asyncio

from aiohttp import web

from planet_tracker import PlanetTracker

tracker = PlanetTracker()
# set some default coordinates
# the following coordinates are for the Greenwich Observatory in the UK
tracker.lon = "-0.0005"
tracker.lat = "51.4769"
tracker.elevation = 0.0

routes = web.RouteTableDef()


@routes.post("/geo_location")
async def geo_location(request):
    print("get_location")
    data = await request.post()

    parsed_data = {
        "lon": str(data["lon"]),
        "lat": str(data["lat"]),
        "elevation": float(data["elevation"])
    }

    tracker.lon = parsed_data["lon"]
    tracker.lat = parsed_data["lat"]
    tracker.elevation = parsed_data["elevation"]
    return web.json_response(parsed_data)


@routes.get("/planets/{name}")
async def get_planet_ephmeris(request):
    planet_name = request.match_info['name']
    print(f"get_planet_ephmeris: {planet_name}")
    planet_data = tracker.calc_planet(planet_name)
    return web.json_response(planet_data)


@routes.get('/')
async def hello(request):
    return web.FileResponse("./index.html")


app = web.Application()
app.add_routes(routes)
app.router.add_static("/", "./")

# web.run_app(app, host="localhost", port=8000)


async def start_app():
    runner = web.AppRunner(app)
    await runner.setup()
    site = web.TCPSite(
        runner, parsed.host, parsed.port)
    await site.start()
    print(f"Serving up app on {parsed.host}:{parsed.port}")
    return runner, site

loop = asyncio.get_event_loop()
runner, site = loop.run_until_complete(start_async_app())
try:
    loop.run_forever()
except KeyboardInterrupt as err:
    loop.run_until_complete(runner.cleanup())
