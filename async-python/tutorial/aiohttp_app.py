# aiohttp_app.py
from aiohttp import web

from planet_tracker import PlanetTracker

tracker = PlanetTracker()
# set some default coordinates
# the following coordinates are for the Greenwich Observatory in the UK
tracker.lon = "-0.0005"
tracker.lat = "51.4769"
tracker.elevation = 0.0

routes = web.RouteTableDef()


@routes.post("/geo_location")
async def geo_location(request):
    print("get_location")
    data = await request.post()

    parsed_data = {
        "lon": str(data["lon"]),
        "lat": str(data["lat"]),
        "elevation": float(data["elevation"])
    }

    tracker.lon = parsed_data["lon"]
    tracker.lat = parsed_data["lat"]
    tracker.elevation = parsed_data["elevation"]
    return web.json_response(parsed_data)


@routes.get("/planets/{name}")
async def get_planet_ephmeris(request):
    planet_name = request.match_info['name']
    print(f"get_planet_ephmeris: {planet_name}")
    planet_data = tracker.calc_planet(planet_name)
    return web.json_response(planet_data)


@routes.get('/')
async def hello(request):
    return web.FileResponse("./index.html")


app = web.Application()
app.add_routes(routes)
app.router.add_static("/", "./")

web.run_app(app, host="localhost", port=8000)
