## Asynchronous Python for web development

### Introduction to Asynchronous Python

### `aiohttp`, an Asynchronous Web Framework

#### Making requests

#### Routes in `aiohttp`

### Serving up PlanetTracker with `aiohttp`

Through the course of this section, I intend to demonstrate how to put together an app
that reports the current coordinates of planets in the sky at the user's location (ephemerides).
The user can supply his or her location manually, or by letting the web Geolocation
API do the work.

#### Planet Ephemerides with pyEphem

- Get coordinates of planets in the sky given some location on Earth.
- Make routes in aiohttp for serving up planet ephemerides.

#### JavaScript Client

- Get user's location with GeoLocation API, and send to server.
- Request and display current azimuth and elevation of planets.

#### Deploying to heroku

### Performance Benefits of using an Asynchronous Framework

#### What do we mean by performance?

#### aiohttp vs requests

#### aiohttp vs Flask
