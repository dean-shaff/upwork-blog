# test_performance.py
import asyncio
import argparse

import aiohttp


async def make_request(session, url):
    async with session.get(url) as resp:
        if resp.status == 200:
            await resp.json()


async def make_requests(n, url):
    async with aiohttp.ClientSession() as session:
        reqs = [make_request(session, url) for i in range(n)]
        await asyncio.ensure_future(
            asyncio.gather(*reqs)
        )


def create_parser():

    parser = argparse.ArgumentParser(description="Test performance of web app")
    parser.add_argument("-n", "--n_requests", default=100, type=int)
    return parser


def main():
    url = "http://localhost:8000/planets/mars"
    parsed = create_parser().parse_args()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(
        make_requests(parsed.n_requests, url)
    )


if __name__ == "__main__":
    main()
